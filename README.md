# Test task progress

DONE:

 * ~~Hello message on blank screen~~
 * Login / Logout page
 * Displaying error messages on login page
 * Autentication using Mongo

NEEDS REWORK (For one-page app):

 * Arbitary user profile page
 * User search page
 * Registration


TODO:

 * Dashboard page with lists of friends, and in/out requests.
 * Accept/Reject friends
 * Clientside: Marionette
 * Divide app.js code to di fferent files


## Run instructions

Prerequisite: 
 
 * Mimosa should be installed
 * You should have mongodb running. 
 * [Windows only]: *Visual Studio Express 2010* should be installed

### To install mimosa:

``npm -g i mimosa``
``mimosa mod:install mimosa-server-reload``

### Runnings project

``npm i`` 
You may need to install VS Express 2010

Running: 
``mimosa watch -s`` 

Open in browser ``localhost:3000``

Try user/password: stub1/stub1
