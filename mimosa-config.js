exports.config = {
    "modules": [
        "copy",
        "jshint",
        "csslint",
        "server",
        "require",
        "minify-js",
        "minify-css",
        "live-reload",
        "server-reload"
    ],
    "app": {
        mongoUri: 'mongodb://127.0.0.1/js-test'
    },
    "watch": {
        compiledDir: "public"
    },
    "server": {
        defaultServer: {
            enabled: false,
            onePager: true
        },
        views: {
            path: 'server/views'
        }
    },
    "serverReload": {
        validate: false
    },
    "bower": {
        copy: {
            strategy: "packageRoot",
            //unknownMainFullCopy: true,
            mainOverrides: {
                "require-handlebars-plugin" : [
                    { "hbs" : "hbs" }
                ]
            }
        }
    },
    "copy": {
        extensions: ["js","css","png","jpg","jpeg","gif","html","eot","svg","ttf","woff","otf","yaml","kml","ico","htc","htm","json","txt","xml","xsd","map","md","mp4","hbs"]
    }
}
