define(['backbone.marionette', 'jquery' ], function(Marionette, $, Controller, Router) {
    var App = new Marionette.Application();
    App.addRegions({
        'mainRegion': ".page-wrapper",
        'navRegion': '.navigation'
    });

    App.on("start", function() {
        if (Backbone.history) {
            Backbone.history.start({
                pushState: true
            });

            if (Backbone.history.fragment === "") {
                Backbone.history.navigate('search', { trigger: true });
            }
        }
        // Auto navigate href starting with #
        $(document).on('click', function(e) {
            var target = e.target.tagName === 'A' ? e.target : null;
            if (!target) {
                target = $(e.target).closest('a')[0];
            }
            if (target && target.hostname === window.location.hostname) {
                Backbone.history.navigate(target.pathname, { trigger: true });
                e.preventDefault();
                return false;
            }
        });
    });

    API = {
        'addFriend' : function() {
            
        }
    };

    App.vent.on("add:friend", function() {
    });

    App.addInitializer(function() {
        this.Profile = {
            friends: [],
            inbox: [],
            outbox: []
        };
    });

    return App;
});

