define(["app", "backbone", "backbone.marionette", "jquery", "underscore"], 
    function (App, Backbone, Marionette, $, _) {
        return App.module("Models", function(Models, App, Backbone, Marionette, $, _) {
            Models.User = Backbone.Model.extend({
                defaults: {
                },
                url: function() {
                    return '/api/users/' + this.id;
                }
            });

            Models.Users = Backbone.Collection.extend({
                url: '/api/users',
                model: Models.User
            });

            var API = {
                getUser: function(username) {
                    var user = new Models.User({
                        id: username
                    });
                    xhr = user.fetch();
                    // save xhr for binding success later in controller
                    user._fetch = xhr;
                    return user;
                }
            };

            App.reqres.setHandler('model:user', function(username) {
                return API.getUser(username);
            });
            
        });
    });
