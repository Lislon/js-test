define(["app", "backbone", "backbone.marionette", "jquery", "underscore"], 
    function (App, Backbone, Marionette, $, _) {
        return App.module("Models", function(Models, App, Backbone, Marionette, $, _) {
            Models.MyProfile = function MyProfile() {
                this.friends = new Backbone.Collection();
                this.inbox = new Backbone.Collection();
                this.outbox = new Backbone.Collection();
            };

            var API = {
                myProfile: null,
                getMyProfile: function() {
                    if (this.myProfile !== null) {
                        return this.myProfile;
                    }
                    this.myProfile = new Models.MyProfile();
                    this.myProfile._fetch = $.get('/api/myprofile', _.bind(function(data) {
                        this.myProfile.friends.reset(data.friends);
                        this.myProfile.inbox.reset(data.inbox);
                        this.myProfile.outbox.reset(data.outbox);
                    }, this));
                    return this.myProfile;
                }
            };

            App.reqres.setHandler('model:my:profile', function() {
                return API.getMyProfile();
            });
            
        });
    });
