define(["app", "backbone", "backbone.marionette", "jquery", "underscore"], 
    function (App, Backbone, Marionette, $, _, TemplateShow) {
        return App.module("Models", function(Models, App, Backbone, Marionette, $, _) {
            App.commands.setHandler("when:fetched", function(models, callback) {
                var xhrs = _.chain([models]).flatten().pluck('_fetch').value();
                $.when.apply($, xhrs).then(callback);
            });
        });
    });
