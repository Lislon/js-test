define(["app", "backbone", "backbone.marionette", "jquery", "underscore", "jquery.cookie"], function (App, Backbone, Marionette, $, _, $cookie) {
    return App.module("App.Session", function(AppSession, App, Backbone, Marionette, $, _) {
        App.addInitializer(function() {
            var redirectToLogin = function () {
                var locationhref = "/auth";
                if (location.hash && location.hash.length > 0) {
                    locationhref += "?hash=" + location.hash.substring(1);
                }
                location.href = locationhref;
            };

            var $doc = $(document);
            $doc.ajaxSend(function (event, xhr) {
                var authToken = $.cookie('connect.sid');
                if (authToken) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                }
            });

            $doc.ajaxError(function (event, xhr) {
                if (xhr.status == 401)
                    redirectToLogin();
            });
        });
    });
});

