define(["hbs/handlebars", "moment"], function (Handlebars, Moment) {
    Handlebars.registerHelper({
        debug: function() {
            console.info(this);
        },
        date: function(date) {
            return Moment(date).format("YYYY/MM/DD");
        }
    });
});
