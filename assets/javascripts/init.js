require.config({
    baseUrl: "/javascripts",
    paths: {
        'jquery': 'vendor/jquery',
        'jquery.cookie': 'vendor/jquery.cookie',
        'underscore': 'vendor/underscore',
        'backbone': 'vendor/backbone',
        'backbone.marionette': 'vendor/backbone.marionette',
        'backbone.wreqr': 'vendor/backbone.wreqr',
        'backbone.babysitter': 'vendor/backbone.babysitter',
        'hbs': 'vendor/hbs',
        'moment': 'vendor/moment'
    },
    hbs: {
        partialsUrl: '/templates'
    }
});

require(['app',
    'apps/navigation/navigation_app',
    'apps/search/search_app',
    'apps/session/session_app',
    'apps/user/user_app',
    'models/user',
    'models/_fetch',
    'models/my_profile',
    'components/templates/helpers'
], function(App) {
    App.start();
});
