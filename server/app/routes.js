var 
    auth = require('../controllers/auth');

module.exports.initialize = function(router, config) {
    router.post('/api/login', auth.login);
    router.get('/', function(req, res) {
        res.render('index', routeOptions);
    });
};
