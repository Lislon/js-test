var passport = require('passport'),
    models = require('./models'),
    passportLocal = require('passport-local');

module.exports.initialize = function(app) {
        app.use(passport.initialize());
        app.use(passport.session());

        passport.use(new passportLocal.Strategy(function (username, password, next) {
            models.User.findOne({ username: username }, function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return next(null, null, { message: 'Unknown user ' + username });
                }
                user.comaprePassword(password, function (err, isMatch) {
                    if (err) {
                        return next(err);
                    }
                    if (isMatch) {
                        return next(null, user);
                    } else {
                        return next(null, null, { message: 'Invalid password' });
                    }
                });
                return null;
            });
        }));

        passport.serializeUser(function (user, done) {
            done(null, user._id);
        });

        passport.deserializeUser(function (id, done) {
            models.User.findOne( { _id: id }, function (err, user) {
                done(err, user);
            });
        });
    };
