var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var paginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var usersSchema = new Schema({
    username: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true },
    firstName: String,
    lastName: String,
    friends: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    inbox: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    outbox: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    registeredAt: { type: Date, 'default': Date.now }
});

// We have to search fast by any combination of first & last name
usersSchema.index({ firstName: 1, lastName: 1});
usersSchema.index({ lastName: 1, firstName: 1});

// Virtual field (Not persisted in Mongo)
usersSchema.virtual('fullName').get(function () {
    return this.firstName + ' ' + this.lastName;
});

var SALT_WORK_FACTOR = 10;

// Intercept saving for hashing password
usersSchema.pre('save', function (next) {
    var user = this;

    // Do rehash only if password modified
    if (!user.isModified('password')) {
        return next();
    }

    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) {
            return next(err);
        }

        // generate hash of password with salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) {
                return next(err);
            }
            // override cleartext pass with the hashed one
            user.password = hash;
            next();
            return null;
        });
        return null;
    });
    return null;
});

// Verify password method
usersSchema.methods.comaprePassword = function (candidatePass, done) {
    bcrypt.compare(candidatePass, this.password, function (err, isMatch) {
        if (err) {
            return done(err);
        }
        done(null, isMatch);
        return null;
    });
    return null;
};

usersSchema.plugin(paginate);

module.exports = {
    User: mongoose.model('user', usersSchema)
};
