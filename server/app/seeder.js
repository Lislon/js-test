var models = require('./models'),
    fakeIdentity = require('fake-identity');

module.exports = {
    check: function () {
        // Generate fake accouts 
        models.User.findOne( { username: 'stub1' }, function (err, user) {
            if (!user) {
                var NUMBER_OF_FAKE_USERS = 500;
                console.log("Creating %s fake users stub1, stub2... stub50", NUMBER_OF_FAKE_USERS);
                for (var i = 0; i < NUMBER_OF_FAKE_USERS; i++) {
                    var fakeName = fakeIdentity.generate();
                    var newUser = new models.User({
                        firstName: fakeName.firstName,
                        lastName: fakeName.lastName,
                        username: 'stub' + i,
                        password: 'stub' + i
                    });
                    newUser.save();
                }
                console.log("Done");
            }
        });
    }
};
