var Models = require('../app/models'),
    _ = require('underscore');

module.exports = {
    show: function(req, res) {
        
        Models.User.findOne({ username: 'stub1' }, function(err, user) {
            var relatedUsers = [].concat(user.friends, user.inbox, user.outbox);
            console.log(relatedUsers);

            Models.User
                .find({ '_id': { $in: relatedUsers } })
                .select('username firstName lastName')
                .exec(_.partial(onRelatedLoad, user));
        });

        var onRelatedLoad = function(user, err, related) {
            var friends = _.filter(related, function(rel) {
                return user.friends.indexOf(rel._id) !== -1;
            });
            var inbox = _(related).filter(function(rel) {
                return user.inbox.indexOf(rel._id) !== -1;
            });
            var outbox = _(related).filter(function(rel) {
                return user.outbox.indexOf(rel._id) !== -1;
            });

            res.send({
                friends: friends,
                inbox: inbox,
                outbox: outbox
            });
        };
    }
}
