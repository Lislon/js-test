var passport = require('passport'),
    models = require('../app/models');

module.exports = {
    auth: function(req, res, next) {
        res.render('auth');
    },
    login: function (req, res, next) {
        passport.authenticate('local', function (err, user, info) {
            if (err) {
                return next(err);
            }
            // Validate input
            req.checkBody('username', 'Username is required').notEmpty();
            req.checkBody('password', 'Password is required').notEmpty();
            var errors = req.validationErrors();
            if (errors) {
                return res.render('auth', { errorText: errors.join(', ') });
            }
            console.log("User logging in: %s", user);

            loginAndAnswerOk = function (user) {
                req.logIn(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    console.log("Success");
                    return res.redirect('/');
                });
            };

            // User/Password pair is not exists
            if (!user) {
                // If user has filled his name, register him
                if (req.body.fullName) {
                    var nameParts = req.body.fullName.split(/\s+/);
                    if (nameParts.length !== 2) {
                        return res.render('auth', { errorText: "Full name should contain 2 words" });
                    }
                    var newUser = new models.User({
                        firstName: nameParts[0].trim(),
                        lastName: nameParts[1].trim(),
                        username: req.body.username.trim(),
                        password: req.body.password
                    });
                    newUser.save(function (err) {
                        if (err) {
                            return next(err);
                        }
                        console.log('Welcome %s [%s]', newUser.fullName, newUser.username);
                        models.User.findOne({ _id: newUser._id }, function (err, user) {
                            if (err) {
                                return next(err);
                            }
                            console.log("New User:");
                            console.log(user);
                            return loginAndAnswerOk(user);
                        });
                    });
                } else {
                    // User hasn't filled his name, just display message
                    console.log("Invalid password (%s)", info.message);
                    return res.render('auth', { errorText: info.message });
                }
            } else {
                // TODO: CChange user name
                return loginAndAnswerOk(user);
            }
        })(req, res, next);
    },
    logout: function (req, res) {
        req.logout();
        if (req.xhr) {
            res.send();
        } else {
            res.redirect('/auth');
        }
    }
}
