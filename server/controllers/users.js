var Models = require('../app/models');

module.exports = {
    show: function(req, res, next) {
        Models.User.findOne({
            username: req.params.username
        }, 'firstName lastName username registeredAt', function(err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                res.send(user);
            } else {
                console.log("User " + req.params.username + " not found");
                res.status(404).end();
            }
        });
    }
};
