var Models = require('../app/models');

function doSearch(query, cb) {
    var directSearch = null,
        reverseSearch = null;
    var nameParts = query.split(/\s+/);

    if (nameParts.length === 1) {
        // Searches like 'James' or 'Bond'
        directSearch = {
            'firstName' : nameParts[0]
        };
        reverseSearch = { 
            'lastName' : nameParts[0] 
        };
    } else {
        // Searches like 'James Bond' or 'Bond James'
        directSearch = { 
            'firstName' : nameParts[0], 
            'lastName' : nameParts[1]
        };
        reverseSearch = { 
            'firstName' : nameParts[1], 
            'lastName' : nameParts[0] 
        };
    }

    var query = { $or: [ directSearch, reverseSearch ]};
    console.log("Search for ", query);
    Models.User.
        find(query).
        limit(1000).
        select('_id username firstName lastName').
        exec(cb);
}

module.exports = {
    search: function(req, res, next) {
        if (req.query.q) {
            doSearch(req.query.q, function(err, results) {
                if (!err) {
                    console.log(err, results);
                    var userFriends = results.map(function(foundUser) {
                        return {
                            fullName: foundUser.fullName,
                            username: foundUser.username,
                            isFriend: foundUser._id in req.user.friends
                        };
                    });
                    res.send({ 'list': userFriends });
                } else {
                    next(req);
                }
            });
        }
    }
};
