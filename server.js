var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var mongoose = require('mongoose');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var morgan = require('morgan');
var fs = require('fs');
var paginate = require('express-paginate');
var moment = require('moment');
var errorHandler = require('errorhandler');
var seeder = require('./server/app/seeder');
var routes = require('./server/app/routes');
var authController = require('./server/controllers/auth');
var mainController = require('./server/controllers/main');
var searchController = require('./server/controllers/search');
var usersController = require('./server/controllers/users');
var myProfileController = require('./server/controllers/my_profile');
var passport = require('./server/app/passport');
var util = require('util');

exports.startServer = function (config, callback) {
    var app = express();
    app.set('views', './server/views');
    app.set('view engine', 'jade');
    app.set('port', process.env.PORT || config.server.port || 3000);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cookieParser());
    app.use(express.static(config.watch.compiledDir));

    app.use(expressSession({
        secret: process.env.SESSION_SECRET || 'secret',
        resave: false,
        saveUninitialized: false
    }));
    app.use(expressValidator());

    app.use(flash());
    app.use(paginate.middleware(10, 50));
    passport.initialize(app);

    if (app.get('env') === 'development') {
        app.use(errorHandler());
    }

    var router = express.Router()


    router.get('/auth', authController.auth);
    router.post('/login', authController.login);
    router.post('/api/logout', authController.logout);
    router.get('/api/search', ensureAutenticated, searchController.search);
    router.get('/api/users/:username', ensureAutenticated, usersController.show);
    //router.get('/api/myprofile', ensureAutenticated, myProfileController.show);
    router.get('/api/myprofile', myProfileController.show);
    router.get('/*', ensureAutenticated, mainController.index)
    app.use('/', router)

    console.log("Connection to db " + config.app.mongoUri + " ...");


    mongoose.connect(config.app.mongoUri, { server: { auto_reconnect: true }}, function(error) {
        if (!error) {
            console.log("Success");
            // Check if database is empty
            seeder.check();
            // oh debug
            require('express-repl')(app);

            // start it up
            app.listen(app.get('port'), function() {
                console.log('Express server listening on port ' + app.get('port'));
            });
        } else {
            console.error("Failed to connect mongoose: " + error);
        }
    });
}

// Middleware to ensure user is authenticated or otherwise send him
// to auth page
function ensureAutenticated(req, res, next) {
    if (!req.isAuthenticated()) {
        console.log(req.originalUrl + " auth? false");
        if (req.xhr) {
            res.status(401).end();
        } else {
            res.redirect('/auth');
        }
    } else {
        next(); 
    }
}
